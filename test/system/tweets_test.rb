require 'application_system_test_case'

class TweetsTest < ApplicationSystemTestCase
  setup do
    @tweet = tweets(:one)
  end

  test 'creating a tweet' do
    visit tweets_url
    click_on 'New Tweet'

    fill_in 'Message', with: @tweet.message
    click_on 'Tweet!'

    assert_text 'Tweet was successfully created!'
  end

  test 'visiting the index' do
    visit tweets_url

    tweets.each do |tweet|
      assert_text tweet.message
    end
  end

  test 'deleting a Tweet' do
    visit tweets_url
    page.accept_confirm do
      click_on 'Delete', match: :first
    end

    assert_text 'Tweet was successfully deleted!'
  end

  test 'creating a tweet with empty message' do
    visit tweets_url
    click_on 'New Tweet'

    click_on 'Tweet!'

    assert_text '1 error prohibited this tweet from being saved:'
  end

  test 'creating a tweet with only spaces in message' do
    visit tweets_url
    click_on 'New Tweet'

    fill_in 'Message', with: '   '
    click_on 'Tweet!'

    assert_text '1 error prohibited this tweet from being saved:'
    assert_text "Message can't be blank"
  end

  test 'creating a tweet with message exceeds 140 characters' do
    visit tweets_url
    click_on 'New Tweet'

    tweet_message = 'this line is consisted of 40 characters ' \
    'this line is consisted of 40 characters ' \
    'this line is consisted of 40 characters ' \
    '21 chars in this line'

    fill_in 'Message', with: tweet_message
    click_on 'Tweet!'

    assert_text '1 error prohibited this tweet from being saved:'
    assert_text 'Message is too long (maximum is 140 characters)'
  end
end
