#!/bin/bash
set -e

rm -f /alitwitter/tmp/pids/server.pid
bundle exec rails db:migrate

exec "$@"
